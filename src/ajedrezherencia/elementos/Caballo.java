/*
 * Copyright 2022 rgcenteno.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ajedrezherencia.elementos;

/**
 *
 * @author rgcenteno
 */
public class Caballo extends Pieza{

    public Caballo(Posicion posicion, boolean blanca) {
        super(posicion, blanca);
    }

    @Override
    public boolean esValidoMovimiento(Posicion p) {
        if(super.esValidoMovimiento(p)){
            int movX = Math.abs(this.getPosicion().getX() - p.getX());
            int movY = Math.abs(this.getPosicion().getY() - p.getY());
            return (movX == 2 && movY == 1) || (movX == 1 && movY == 2);
        }
        else{
            return false;
        }
    }
    
    @Override
    public String saludar() {
        return "Soy un caballo " + (this.isBlanca() ? "blanco" : "negro") + " situada en la posición [" + this.getPosicion() + "]";
    }
    
}
