/*
 * Copyright 2022 rgcenteno.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ajedrezherencia.elementos;

import com.google.common.base.Preconditions;
/**
 *
 * @author rgcenteno
 */
public abstract class Pieza {
    
    private static int movimientosValidos = 0;
    private static int movimientosInvalidos = 0;
    private Posicion posicion;
    private final boolean blanca;

    protected Pieza(Posicion posicion, boolean blanca) {
        Preconditions.checkNotNull(posicion);
        this.posicion = posicion;
        this.blanca = blanca;
    }    

    public boolean isBlanca() {
        return blanca;
    }
    
    public boolean esValidoMovimiento(Posicion p){
        if(this.posicion == null){
            return false;
        }
        if(p == null){
            return false;
        }
        else if(this.posicion.equals(p)){
            return false;
        }
        return true;
    }
    
    public boolean mover(Posicion p){
        if(esValidoMovimiento(p)){
            this.posicion = p;
            movimientosValidos++;
            return true;
        }
        else{
            movimientosInvalidos++;
            return false;
        }
    }

    protected Posicion getPosicion() {
        return posicion;
    } 

    public static int getMovimientosValidos() {
        return movimientosValidos;
    }

    public static int getMovimientosInvalidos() {
        return movimientosInvalidos;
    }        
    
    public abstract String saludar();
        
}
