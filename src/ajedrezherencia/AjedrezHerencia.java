/*
 * Copyright 2022 rgcenteno.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ajedrezherencia;

import ajedrezherencia.elementos.*;
/**
 *
 * @author rgcenteno
 */
public class AjedrezHerencia {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        java.util.List<Pieza> lista = new java.util.LinkedList<>();
        lista.add(new Torre(new Posicion(1,1), true));
        lista.add(new Alfil(new Posicion(2,2), true));
        lista.add(new Caballo(new Posicion(3,3), true));
        
        Pieza p = lista.get(0); //Torre
        System.out.println(p.saludar());
        System.out.println("Movemos torre a 3,1 :" + p.mover(new Posicion(3,1)));
        System.out.println(p.saludar());
        System.out.println("Movemos torre a 4,2 :" + p.mover(new Posicion(4,2)));
        System.out.println(p.saludar() + "\n\n");
        
        p = lista.get(1); //Alfil
        System.out.println(p.saludar());
        System.out.println("Movemos alfil a 4,4 :" + p.mover(new Posicion(4,4)));
        System.out.println(p.saludar());
        System.out.println("Movemos alfil a 5,4 :" + p.mover(new Posicion(5,4)));
        System.out.println(p.saludar() + "\n\n");
        
        p = lista.get(2);
        System.out.println(p.saludar());
        System.out.println("Movemos caballo a 4,5 :" + lista.get(2).mover(new Posicion(4,5)));
        System.out.println(p.saludar());
        System.out.println("Movemos caballo a 5,6 :" + lista.get(2).mover(new Posicion(5,4)));
        System.out.println(p.saludar() + "\n\n");
        
        System.out.println("Movimientos válidos: " + Pieza.getMovimientosValidos());
        System.out.println("Movimientos inválidos: " + Pieza.getMovimientosInvalidos());
        
    }
    
}
