Vamos a crear una aplicación que nos permita saber si una pieza de ajedrez puede moverse a
una Posición especificada.
-Posición será una Clase que tendrá dos atributos enteros X e Y.
-Todas las piezas del ajedrez tendrán:
• Atributo posición de la clase Posición que indica la posición donde se encuentra.
• Atributo para saber si es blanca o negra.
• Constructor que inicialice ambas variables.
• Un método booleano validarMovimiento(Posición testPosition) que comprueba si
podemos mover la ficha especificada a una posición dada.
o La Posición será inválida si x, y no están entre [1...8]
o Además, cada tipo de pieza validará si el movimiento está permitido.
Apartados:
a. Desarrollar la clase Pieza.
b. Desarrollar la clase Alfil. Un alfil se puede mover sólo en diagonal tanto hacia adelante
como hacia atrás.
c. Desarrollar la clase Torre. Una torre sólo se puede mover en línea recta (eje x o eje y).
d. Necesitamos tener una variable que nos permita saber cuántas piezas hemos podido
mover y cuantos movimientos erróneos han propuesto los usuarios.
e. ¿Se debería poder crear un objeto del tipo Figura en un ajedrez? Modifica la aplicación
en base a tu respuesta.
f. Obliga a todas las figuras a que implementen un método saludar() que devuelve un
string con su descripción. Por ejemplo, una Torre negra devolverá: “Soy una Torre
negra situada en la posición [X, Y]”
